try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import tkMessageBox
except ImportError:
    from tkinter import messagebox as tkMessageBox


class QuotePublisher:

    class __QuotePublisher:
        @staticmethod
        def publish(todayPrice):
            root = tk.Tk()
            root.overrideredirect(1)
            root.withdraw()
            tkMessageBox.showinfo(title="Big fail!", message="You've pushed a dummy auction to a real ads platform, the business is upset!")

    instance = __QuotePublisher()

    def __init__(self):
        pass
