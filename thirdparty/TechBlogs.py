# coding=utf-8
import random
import time


class TechBlogs:

    @staticmethod
    def list_all_blogs():
        time.sleep(7)  # do not remove this line, Access to DB are very slow

        rv = [
            'HackerNews', 'Reddit', 'TechCrunch', 'BuzzFeed', 'TMZ', 'TheHuffPost', 'GigaOM', 'LinuxFR',
            '3 Quarks Daily', 'AirlineReporter.com', 'Aangilam', '1000 Awesome Things', 'The 9 on Yahoo!',
            'A Motley Vision', 'The Art Life', 'Athletics Nation', 'Audimated', 'Bad Astronomy',
            'B2Blogger.com', 'Balkinization', 'The Bear Club', 'Belle de Jour', 'Belmont Club',
            'Bestiario del balón', 'Blog del Narco', 'Blogcritics', 'Bloggingheads.tv',
            'The "Blog" of "Unnecessary" Quotation Marks', 'The Blonde Salad', 'Boing Boing',
            'Borderland Beat', 'Brain Blogger', 'The Brussels Journal', 'Cake Wrecks', 'CampusJ',
            'Chinadialogue', 'Climate Audit', 'The Comics Curmudgeon', 'ConservativeHome', 'Consumerist',
            'Contempo Magazine', 'Core77', 'Cosmic Variance', 'Couric & Co.', 'Crash Test Kitchen',
            'Crooked Timber', 'Crooks and Liars', 'Cute Overload', 'The Daily Howler', 'The Daily Nightly',
            'Daring Fireball', 'Deadspin', 'DesiPundit', 'Diabetes Mine', 'Dlisted', 'Down By The Hipster',
            'DrugWarRant', 'Durma Melhor', 'EastSouthWestNorth', 'Engadget', 'EQ Music', 'Faces in Places',
            'Film', 'Fluxblog', 'FoodMayhem', 'Front Porch Republic', 'GamePolitics.com', 'Gawker',
            'GeenStijl', 'Generación invisible', 'Gigaom', 'Gizmodo', 'Go Fug Yourself',
            'Google Blogoscoped', 'Gothamist', 'Groklaw', 'Grumpy Old Bookman', 'Gulfsails', 'Hollaback!',
            'Hot Air', 'How the World Sees America', 'The Huffington Post', 'I Can Has Cheezburger?',
            'IPKat', 'Idolblog', 'The Incidental Economist', 'India Uncut', 'Inhabitat', 'InkTank',
            'IvyGate', 'Joystiq', 'Jezebel', 'Jihad Watch', 'Kaboom: A Soldier\'s War Journal',
            'Kissing Suzy Kolber', 'Kotaku', 'Language Log', 'Law and the Multiverse',
            'Lebanese Political Journal', 'Liberty and Power', 'Lifehacker', 'Little Green Footballs',
            'Marginal Revolution', 'The Marmot\'s Hole', 'Mashable', 'Memepool', 'MetaFilter',
            'Metro Jacksonville', 'Milblogging.com', 'Mind Hacks', 'Mini-Microsoft', 'MobuzzTV',
            'Momversation', 'Monsters and Critics', 'Mr. Money Mustache', 'MSDN Blogs', 'Music for Robots',
            'MyDD', 'Node Magazine', 'The Oil Drum', 'OpEdNews', 'Overheard in New York',
            'Overheard in Pittsburgh', 'The Panda\'s Thumb', 'Patentlyo', 'Patribotics', 'Pharyngula',
            'PinoyCentric', 'Politically Incorrect', 'PopText', 'Popjustice', 'PopSugar', 'ProBlogger.com',
            'Protein Wisdom', 'The Psycho Ex-Wife', 'QT\'s Diary', 'Rate Your Students', 'ReadWrite',
            'RealClimate', 'Retecool', 'Rocketboom', 'Sarcastic Gamer', 'The Sartorialist',
            'The Scientific Activist', 'SCOTUSblog', 'Seeking Alpha', 'Sense on Cents',
            'the show with zefrank', 'Simply Recipes', 'Skatter Tech', 'Slugger O\'Toole',
            'Smashing Magazine', 'The Sneeze', 'The Soxaholix', 'Squawk Box', 'Stuff White People Like',
            'Surviving Grady', 'TV Newser', 'TechCrunch', 'The Tehran Times', 'Terra Nova', 'TorrentFreak',
            'TPMCafe', 'TrueHoop', 'The Underwear Expert', 'Valleywag', 'Velvet Underground', 'Vlogbrothers',
            'The Volokh Conspiracy', 'Watts Up With That?', 'Wise Bread', 'Worldchanging', 'Yummly',
            'Zen Habits', 'Zooillogix', ]
        random.shuffle(rv)
        return rv
